
TESTDIR=${1}
GIT_REVNUM=`git rev-parse --short HEAD`
DATE=`date "+%F-%T"`
FILE=stats/${DATE}.stats
ARGS='PROGNAME="release" NPHOTONS=10000000 NITERATIONS=500'

cd "$TESTDIR"

echo Performance test for rev ${GIT_REVNUM}
if [ ! -d stats ]; then mkdir stats; fi
echo ${GIT_REVNUM} > ${FILE}
echo ${ARGS} >> ${FILE}
for i in 1 2 3; do
    echo "#"$i >> ${FILE}
    make run ${ARGS} | grep solve | tail -n1 >> ${FILE}
done

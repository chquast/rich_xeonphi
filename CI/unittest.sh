#!/bin/bash

set -e

TESTDIR=${1}

cd "$TESTDIR"
BUILD=$2

echo Running unittests for build "$BUILD" in $(pwd)
make && make run

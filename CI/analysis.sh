#!/bin/bash

set -e
set -x

TESTDIR=${1}

cd "$TESTDIR"

if [ -s asan_debug ]
then
    make run PROGNAME="asan_debug"
fi

if [ -s asan_release ]
then
        make run PROGNAME="asan_release"
if

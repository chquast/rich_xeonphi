#include <iostream>
#include <complex>
#include <cmath>
#include <omp.h>

#define N 1000000

int main()
{
    double start, stop;
    auto w1 = std::complex<float>(0.0);
    auto w2 = std::complex<float>(0.0);
    float u1 = -1, u2 = -2;
    auto  V = std::complex<float>(0.0);

    start = omp_get_wtime();
    for (int i = 0; i <N; i++) {
        w1 = std::sqrt( std::complex<float>(u1, u2) );
        w2 = std::sqrt( std::complex<float>(u1,-u2) );
        V = w1 * w2;
    }
    stop = omp_get_wtime();
    std::cout << "V: " << V << std::endl;
    std::cout << "w1 real: " <<  std::real(w1)  << std::endl;
    std::cout << "w2 real: " <<  std::real(w2)  << std::endl;
    std::cout << "time: " <<  (stop-start)  << std::endl;

    auto w1r = 0.0;
    auto w2r = 0.0;
    auto V2 = 0.0;
    start = omp_get_wtime();
    for (int i = 0; i <N; i++) {
        V2 = std::sqrt(u1*u1 + u2*u2);
        w1r = std::sqrt((u1+std::sqrt(u1*u1+u2*u2))/2);
        w2r = w1r;
    }
    stop = omp_get_wtime();
    std::cout << "V2: " << V2 << std::endl;
    std::cout << "w1 real: " <<  std::real(w1r)  << std::endl;
    std::cout << "w2 real: " <<  std::real(w2r)  << std::endl;
    std::cout << "time: " <<  (stop-start)  << std::endl;
}

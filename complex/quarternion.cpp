#include <iostream>
#include <omp.h>
#include <array>

// VectorClass
#define MAX_VECTOR_SIZE 512
#include "VectorClass/complexvec.h"
#include "VectorClass/vectorclass.h"


#pragma GCC push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

// STL
#include <math.h>
#include <type_traits>
//#include <complex>

// Eigen
#include "LHCbMath/EigenTypes.h"
#include <Eigen/Geometry>

// VDT
#include "vdt/asin.h"
#include "vectormath_trig.h"

// VectorClass
#include "VectorClass/vectorclass.h"
#include "VectorClass/complexvec.h"
#include "VectorClass/complexvec.h"

// LHCb Maths
#include "LHCbMath/FastRoots.h"
#pragma GCC pop

#define N 10000
using Vector = LHCb::Math::Eigen::XYZVector;
using Eigen3Vector = Eigen::Matrix< float, 3 , 1 >;

#define PR(a)
//#define PR(a)   do { std::cout << #a << ": " << a << std::endl; } while(0);

void oldRICH(float sinbeta, float e, const Vector evec, const Vector dvec,
                Gaudi::XYZPoint CoC, float radius, Gaudi::XYZPoint& sphReflPoint) {
    // (normalised) normal vector to reflection plane
    auto n = evec.cross3(dvec);
    n /= std::sqrt( n.dot(n) );

    // construct rotation transformation
    // Set vector magnitude to radius
    // rotate vector and update reflection point
    //std::cout << "n: " << n << std::endl;
    //std::cout << "beta: " << vdt::fast_asinf(sinbeta) << std::endl;
    const Eigen::AngleAxis<float> angleaxis( vdt::fast_asinf(sinbeta),
            Eigen3Vector(n[0],n[1],n[2]));
    auto erad = Eigen3Vector(evec[0],evec[1],evec[2]) *( radius / e );
    //std::cout << "erad: " << erad << std::endl;
    auto Mvec = Gaudi::XYZVector( angleaxis * erad);
    //std::cout << "Mvec: " << Mvec << std::endl;
    sphReflPoint = ( CoC + Mvec );

}

void newRICH(Vec16f const sinbeta, Vec16f e,
                Vec16f evecX, Vec16f evecY, Vec16f evecZ,
                Vec16f dvecX, Vec16f dvecY, Vec16f dvecZ,
                Vec16f CoCX, Vec16f CoCY, Vec16f CoCZ,
                Vec16f radius,
                Vec16f& srefPX, Vec16f& srefPY,Vec16f& srefPZ ) {
    //auto n = evec.cross3(dvec);
    auto nx = (evecY*dvecZ) - (evecZ*dvecY);
    auto ny = (evecZ*dvecX) - (evecX*dvecZ);
    auto nz = (evecX*dvecY) - (evecY*dvecX);

    auto norm = (nx*nx+ny*ny+nz*nz);
    auto divnorm = 1.0f/(nx*nx+ny*ny+nz*nz);
    auto norm_sqrt = sqrt(norm);
    nx *= divnorm;
    ny *= divnorm;
    nz *= divnorm;
    // FIXME: is there a more performant asin function?
    // FIXME: where else to add const?
    // FIXME: alignment
     auto beta = asin(sinbeta);
    PR(beta[0]);

    auto a = sinbeta*norm_sqrt;
    auto b = (1.0f-cos(beta))*(norm);
    auto enorm = radius/e;
    // symmetric matrix, diagonal part is 0
    const std::array<Vec16f, 9> M = {1+b*(-nz*nz-ny*ny), -a*nz+nx*ny, a*ny-b*nx*nz,
                                    a*nz-b*nx*ny, 1-b*(-nx*nx-nz*nz), -a*nx+b*ny*nz,
                                    -a*ny+b*nx*nz, a*nx+b*ny*nz, 1+b*(-ny*ny-nx*nx)};

    const auto ex = enorm*(evecX*M[0]+evecY*M[1]+evecZ*M[2]);
    const auto ey = enorm*(evecX*M[3]+evecY*M[4]+evecZ*M[5]);
    const auto ez = enorm*(evecX*M[6]+evecY*M[7]+evecZ*M[8]);

    srefPX = {ex + CoCX};
    srefPY = {ey + CoCY};
    srefPZ = {ez + CoCZ};
}

int main()
{
    float sinbeta = 0.10803;
    float n[3] = {0.224832, -0.972898, 0.0540296};
    float e = 6722.1;
    const Vector evec(-566.164,-67.0076, 6697.88);
    const Vector dvec(-3070.31,-5.50764,4735.57);
    Gaudi::XYZPoint CoC(116.498,17.3877, 3367.89);
    float radius = 8538.34;
    Gaudi::XYZPoint sphReflPoint = Gaudi::XYZPoint(-1967,-43.7518,11647.9);

    Vec16f Vsinbeta = {0.10803};
    Vec16f Vnx = {0.224832};
    Vec16f Vny = {-0.972898};
    Vec16f Vnz = {0.0540296};
    Vec16f Ve = {6722.1};

    Vec16f VevecX(-566.164);
    Vec16f VevecY(-67.0076);
    Vec16f VevecZ(6697.88);

    Vec16f VdvecX(-3070.31);
    Vec16f VdvecY(-5.50764);
    Vec16f VdvecZ(4735.57);

    Vec16f VCoCX(116.498);
    Vec16f VCoCY(17.3877);
    Vec16f VCoCZ(3367.89);

    Vec16f Vradius = 8538.34;

    Vec16f VsrefPX, VsrefPY, VsrefPZ;

    double start, stop;
    start = omp_get_wtime();
    for (int i = 0; i <N; i++) {
        oldRICH(sinbeta, e, evec, dvec, CoC, radius, sphReflPoint);
    }
    stop = omp_get_wtime();

    std::cout << "old reflPoint: " << sphReflPoint << std::endl;
    std::cout << "time: " <<  (stop-start)  << std::endl;

    start = omp_get_wtime();
    for (int i = 0; i <N/16; i++) {
        newRICH(Vsinbeta, Ve, VevecX, VevecY, VevecZ, VdvecX, VdvecY, VdvecZ,
                    VCoCX, VCoCY, VCoCZ, Vradius, VsrefPX, VsrefPY, VsrefPZ );
    }
    stop = omp_get_wtime();

    std::cout << "new reflPoint: " << VsrefPX[0] << ", " << VsrefPY[0] << ", " << VsrefPZ[0]  << std::endl;
    std::cout << "time: " <<  (stop-start)  << std::endl;
}

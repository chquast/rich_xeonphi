import numpy as np
import math
import cmath as cm

def sinbeta_original(a, b, c, d):
        r4 = 1.0 / 4.0;
        q2 = 1.0 / 2.0;
        q8 = 1.0 / 8.0;
        q1 = 3.0 / 8.0;
        q3 = 3.0 / 16.0;
        UU = -0.866025404; # - sqrt(3)/2

        aa = a * a;
        pp = b - q1 * aa;
        qq = c - q2 * a * (b - r4 * aa);
        rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
        rc = q2 * pp;
        sc = r4 * (r4 * pp * pp - rr);
        tc = -(q8 * qq)**2;

        #print([a, b, c])
        qcub = (rc * rc - 3 * sc);
        rcub = (2.0 * rc * rc * rc - 9 * rc * sc + 27.0 * tc);

        Q = qcub / 9.0;
        R = rcub / 54.0;

        Q3 = Q * Q * Q;
        R2 = R * R;

        sgnR = -np.sign(R) ;

        A = sgnR * ( abs(R) + math.sqrt(abs(R2-Q3)))**(1./3.);

        B = Q / A;

        u1 = -0.5 * (A + B) - rc / 3.0;
        u2 = UU * abs(A-B);

        w1 = cm.sqrt(complex(u1, u2));
        w2 = cm.sqrt(complex(u1,-u2));
        V = w1 * w2;
        w3 = (( qq * -0.125 ) / V if  abs(V) != 0.0 else complex(0,0) );
        res = w1.real + w2.real + w3.real - (r4*a);

        #print([w1, w2, V])
        return (res if -0.1 < res < 1.0 else np.sign(res))


def sinbeta_vect(a, b, c, d):
        r4 = 1.0 / 4.0;
        q2 = 1.0 / 2.0;
        q8 = 1.0 / 8.0;
        q1 = 3.0 / 8.0;
        q3 = 3.0 / 16.0;
        UU = -0.866025404; # - sqrt(3)/2

        aa = a * a;
        pp = b - q1 * aa;
        qq = c - (q2 * a * (b - (r4 * aa)));
        rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
        rc = q2 * pp;
        sc = r4 * (r4 * pp * pp - rr);
        tc = -(q8 * qq)**2;

        qcub = (rc * rc - 3 * sc);
        rcub = (2.0 * rc * rc * rc - 9 * rc * sc + 27.0 * tc);

        Q = qcub / 9.0;
        R = rcub / 54.0;

        Q3 = Q * Q * Q;
        R2 = R * R;

        sgnR = 1
        if  R >= 0:
            sgnR = -1

        toberooted = (abs(R) + math.sqrt(abs(R2-Q3)) );

        A = sgnR * (toberooted)**(1./3.);

        B = Q / A;

        u1 = -0.5 * (A + B) - rc / 3.0;
        u2 = UU * abs(A-B);

        V = math.sqrt(u1*u1 + u2*u2);
        w3r = 0.0
        if V != 0.0:
            w3r = (qq * -0.125)/V

        res = math.sqrt((u1+math.sqrt(u1*u1+u2*u2))/2)*2 + w3r - (r4*a);

        #print([w1, w2, V])
        return (res if -0.1 < res < 1.0 else np.sign(res))

if __name__ == "__main__":
    veca = [-0.332045, -0.280191, -0.673648, -0.564775, -0.272349, -0.479625, -0.370134, -0.137196, -0.488955, -0.293671, -0.27643, -0.783628, -0.579528, -0.690491, -0.514255, -0.725753, ]
    vecb = [0.767185, 1.06075, 0.571904, 0.754278, 1.36696, 0.921044, 0.90598, 1.18719, 1.07204, 0.765942, 1.29331, 1.10428, 1.10696, 0.716667, 0.547838, 1.07706, ]
    vecc = [0.0413242, 0.039994, 0.0748828, 0.0642459, 0.0397705, 0.0612036, 0.0484879, 0.0195824, 0.0684753, 0.0413782, 0.0413912, 0.145675, 0.0900579, 0.0912625, 0.0602329, 0.146133, ]
    vecd = [-0.00427013, -0.00327598, -0.0300165, -0.0189434, -0.00377565, -0.0119569, -0.00602441, -0.000834959, -0.0122321, -0.00246963, -0.00355971, -0.025632, -0.0160766, -0.0277372, -0.0106405, -0.0147812, ]
    sb = [0.0527208, 0.0399547, 0.183694, 0.12494, 0.0400844, 0.0866211, 0.0594832, 0.0195384, 0.0804394, 0.0359935, 0.0389462, 0.102022, 0.0876867, 0.149072, 0.0971549, ]

    print("*** Original ****")
    for a, b, c, d, s in zip(veca, vecb, vecc, vecd, sb):
        sinbeta = sinbeta_original(a, b, c, d)
        if (sinbeta-s)>1.0E-7:
            print("sinbeta is ", sinbeta, ", should be ", s, "diff: ", sinbeta-s)

    print("*** Vectorized ****")
    for a, b, c, d, s in zip(veca, vecb, vecc, vecd, sb):
        sinbeta = sinbeta_vect(a, b, c, d)
        if (sinbeta-s)>1.0E-7:
            print("sinbeta is ", sinbeta, ", should be ", s, "diff: ", sinbeta-s)


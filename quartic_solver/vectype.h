
#ifndef __VECTYPE__
#define __VECTYPE__

#include <vector>
#include <array>
#include "aligned_allocator.h"

namespace VECTYPE {
    template <typename T, std::size_t DIM = 16>
        class XYZPoints
        {
            public:
                enum { N = DIM };
                static constexpr std::size_t size() { return N; }
                typedef T value_type;
                typedef std::array<T, N> vec_type;
            private:
                std::array<vec_type, 3> vec;
            public:
                vec_type& x() { return vec[0]; }
                const vec_type& x() const { return vec[0]; }
                vec_type& y() { return vec[1]; }
                const vec_type& y() const { return vec[1]; }
                vec_type& z() { return vec[2]; }
                const vec_type& z() const { return vec[2]; }
        };

    template <typename T, std::size_t DIM = 16>
        class PhotonReflection
        {
            public:
                typedef typename XYZPoints<T, DIM>::vec_type vector;
            public:
                XYZPoints<T, DIM> emissPnt;
                XYZPoints<T, DIM> centOfCurv;
                XYZPoints<T, DIM> virtDetPoint;
                // FIXME: rausnehmen, eigene datenstruktur:
                XYZPoints<T, DIM> sphReflPoint;
                // FIXME: align data to cache line boundary
                // FIXME: how to pass array size properly?
                std::array<T,DIM> radius;
        };

    template <typename T, std::size_t DIM = 16>
        using PhotonReflections = std::vector<PhotonReflection<T, DIM>,  aligned_allocator< PhotonReflection<T, DIM>, 64>>;

    template < class T>
        std::ostream& operator<< (std::ostream& os, const XYZPoints<T>& vp) {
            for (int i = 0; i < vp.size(); i++) {
                // FIXME: will ich nicht eher die zeilen sehen?
                // os << i << ": x=" << vp.x()[i] << ", y=" << vp.y()[i] << ", z=" << vp.z()[i] << std::endl;
                os << "(" << vp.x()[i] << "," << vp.y()[i] << "," << vp.z()[i] << ")" << std::endl;
            }
            return os;
        }

    template < typename T>
        std::ostream& operator<<(std::ostream& os, const VECTYPE::PhotonReflection<T> & dat) {
            return os   << "emissPnt: " << dat.emissPnt
                << "centOfCurv:  " << dat.centOfCurv
                << "virtDetPoint:  " << dat.virtDetPoint
                << "radius:  " << dat.radius
                << std::endl;
        }

    template < typename T>
        std::ostream& operator<<(std::ostream& os, const typename VECTYPE::PhotonReflections<T>::Vector & vt) {
            std::cout << "VECTYPE::Data::Vector " << std::endl;
            for (auto const& val : vt) {
                os << val << std::endl;
            }
            return os;
        }
}

#endif  /*  __VECTYPE__ */

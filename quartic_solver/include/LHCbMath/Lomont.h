// $Id: Lomont.h 124901 2011-06-16 12:46:28Z ibelyaev $
// ============================================================================
#ifndef LHCBMATH_LOMONT_H 
#define LHCBMATH_LOMONT_H 1
// ============================================================================
// Incldue files 
// ============================================================================
#include "GaudiKernel/Lomont.h"
// ============================================================================
namespace LHCb
{
  // ==========================================================================
  namespace Math 
  {
    // ========================================================================
    using Gaudi::Math::lomont_compare_float  ;
    using Gaudi::Math::lomont_compare_double ;
    using Gaudi::Math::next_float            ;
    using Gaudi::Math::next_double           ;
    // ========================================================================
  } //                                              end of namespace LHCb::Math
  // ==========================================================================
} //                                                      end of namespace LHCb
// ============================================================================
// The END 
// ============================================================================
#endif // LHCBMATH_LOMONT_H
// ============================================================================

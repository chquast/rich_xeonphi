#!/bin/bash

INTEL_PATH=/cvmfs/projects.cern.ch/intelsw/psxe/linux/all-setup.sh
CMD=amplxe-cl
#PARAM="-collect memory-access"
#PARAM="-collect advanced-hotspots"
PARAM="-collect general-exploration"
#PROG="./release 10000000 100"
PROG="./release 10485760 1000 2048"
command $CMD -v || source $INTEL_PATH

if [[ $# -ne 0 ]]; then
	REPORT_NAME="-r $1"
fi

#export OMP_NUM_THREADS=240
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/ROOT/6.04.02/x86_64-slc6-gcc48-opt/lib

full_cmd="$CMD $PARAM $REPORT_NAME $PROG"
echo $full_cmd
$full_cmd

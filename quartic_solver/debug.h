#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <chrono>

#ifdef DEBUG_SOLVE
#define PR(a)  if (first == true) do { std::cout << #a << ": " << (a) << std::endl; } while(0);
#define PR0(a)   if (first == true) do { std::cout << #a << ": " << a[0] << std::endl; } while(0);
#define PRv(a)  if (first == true) do { std::cout << #a << ": (" << a##X[0] << "," << a##Y[0] \
    << ", " <<  a##Z[0] << ")" << std::endl; } while(0);
#else
#define PR(a)
#define PR0(a)
#define PRv(a)
#endif  /* DEBUG_SOLVE */

#ifdef TIMERS
//std::chrono::duration_cast<std::chrono::nanoseconds> timer_quartic;
auto timer_transform = std::chrono::nanoseconds();
auto timer_quartic= std::chrono::nanoseconds();
auto timer_cbrt = std::chrono::nanoseconds();
// std::chrono::high_resolution_clock::time_point timer_transform;
// std::chrono::high_resolution_clock::time_point timer_cbrt;

#define TIMER_START(timername) \
    const auto timername##_start = std::chrono::high_resolution_clock::now();

#define TIMER_STOP(timername)  \
    const auto timername##_stop = std::chrono::high_resolution_clock::now(); \
timername = timername + std::chrono::duration_cast<std::chrono::nanoseconds>(timername##_stop -  timername##_start);

#else
#define TIMER_START(timername)
#define TIMER_STOP(timername)
#endif  /* TIMERS */

/* Needed for Intel SDE  */
#include <stdio.h>
#include <stdlib.h>

#if defined( __INTEL_COMPILER)
#define INSERT_SSC_MARK(tag) __SSC_MARK(tag)
#else
#define INSERT_SSC_MARK(tag)                                          \
__asm__ __volatile__ ("movl %0, %%ebx; .byte 0x64, 0x67, 0x90 " ::"i"(tag):"%ebx")
#endif	/* __INTEL_COMPILER */


#endif  /* _DEBUG_H_ */

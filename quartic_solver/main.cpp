#pragma GCC push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define MAX_VECTOR_SIZE 512
#pragma GCC pop

// Quartic Solver
#include "QuarticSolver.h"
#include "QuarticSolverNew.h"
#include "QuarticSolverMath.h"
#include "QuarticSolverCacheline.h"
#include "QuarticSolverCachelineReduced.h"
#include "QuarticSolverCachelineGCC.h"

#include "vectype.h"
#include "vectype_reduced.h"


// STL
#include <random>
#include <vector>
#include <iostream>
#include <string>
#include <typeinfo>
#include <chrono>

#include "debug.h"

uint64_t nIterations = NITERATIONS;
uint64_t nPhotons = NPHOTONS;
uint64_t nchunks = NCHUNKS;

#define POOLSIZE 50
#define PARA 16

// Make an instance of the quartic solver
Rich::Rec::QuarticSolver qSolver;
Rich::Rec::QuarticSolverNew qSolverNew;
Rich::Rec::QuarticSolverNewton qSolverNewton;


RichCacheline::Rec::QuarticSolverCacheline qSolverCacheline;
RichCachelineGCC::Rec::QuarticSolverCachelineGCC qSolverCachelineGCC;

template <typename T>
class ConstDataGenerator
{
    	private:
		typedef typename RichCachelineReducedData::Rec::QuarticSolverCacheline<T>::XYZPoint XYZPoint;
		std::vector<XYZPoint>  EmissPntPool;
		std::vector<T>  RadiusPool;
		std::vector<XYZPoint>  CoCPool;


	public:
		ConstDataGenerator() { }
		ConstDataGenerator(unsigned int seed)
		{
    		static std::default_random_engine gen(seed);

			EmissPntPool.reserve(POOLSIZE);
			CoCPool.reserve(POOLSIZE);
			RadiusPool.reserve(POOLSIZE);

			static std::uniform_real_distribution<double> r_emiss_x(-800,800), r_emiss_y(-600,600), r_emiss_z(10000,10500);
			static std::uniform_real_distribution<double> r_coc_x(-3000,3000), r_coc_y(-20,20),     r_coc_z(3300,3400);
			static std::uniform_real_distribution<float>  r_rad(8500,8600);

			for(int i = 0; i < POOLSIZE; i++) {
				EmissPntPool.push_back(XYZPoint{ r_emiss_x(gen), r_emiss_y(gen), r_emiss_z(gen) });
				CoCPool.push_back(XYZPoint{r_coc_x(gen),   r_coc_y(gen),   r_coc_z(gen)});
				RadiusPool.push_back(r_rad(gen));
			}
			
		}
	
		std::vector<XYZPoint> getEmissPntPool() {
			return EmissPntPool;
		}
	
		std::vector<T> getRadiusPool() {
			return RadiusPool;
		}
	
		std::vector<XYZPoint> getCoCPool() {
			return CoCPool;
		}
};


ConstDataGenerator<float> constDat(42);
RichCachelineReducedData::Rec::QuarticSolverCacheline<float> qSolverCachelineReduced(constDat.getEmissPntPool(), constDat.getRadiusPool(), constDat.getCoCPool());

class Data
{
public:
  typedef std::vector<Data> Vector;
public:
  Gaudi::XYZPoint emissPnt;
  Gaudi::XYZPoint centOfCurv;
  Gaudi::XYZPoint virtDetPoint;
  double           radius;
  Gaudi::XYZPoint sphReflPoint;
public:
  Data(unsigned int seed) 
  {
    // randomn generator
    static std::default_random_engine gen(seed);
    // TODO: time als seed? gen.seed(4);
    // Distributions for each member

    static std::uniform_real_distribution<double> r_vdp_x(-3000,3000), r_vdp_y(-200,200),   r_vdp_z(8100,8200);
    virtDetPoint = Gaudi::XYZPoint( r_vdp_x(gen),   r_vdp_y(gen),   r_vdp_z(gen)   );

    static std::uniform_real_distribution<double> r_emiss_x(-800,800), r_emiss_y(-600,600), r_emiss_z(10000,10500);
    static std::uniform_real_distribution<double> r_coc_x(-3000,3000), r_coc_y(-20,20),     r_coc_z(3300,3400);
    static std::uniform_real_distribution<float>  r_rad(8500,8600);
    emissPnt     = Gaudi::XYZPoint( r_emiss_x(gen), r_emiss_y(gen), r_emiss_z(gen) );
    centOfCurv   = Gaudi::XYZPoint( r_coc_x(gen),   r_coc_y(gen),   r_coc_z(gen)   );
    radius       = r_rad(gen);
  }
};

template< class TYPE, std::size_t DIM = 16 >
static void shuffle_reduced(const Data::Vector& dataV0 , typename VECTYPE_REDUCED::PhotonReflections<TYPE, DIM>& dataV1) {
    size_t len = dataV0.size();
    // std::cout << len << std::endl;
    // FIXME: Unterschied ob ich lesend oder schreibend springe?
    // FIXME: auto& ?
    auto permpart = [&] (auto& tmpfield, int i, const auto& datafield) {
        tmpfield.x()[i] = datafield.x();
        tmpfield.y()[i] = datafield.y();
        tmpfield.z()[i] = datafield.z();
    };
    auto perm = [=, &dataV0] (VECTYPE_REDUCED::PhotonReflection<TYPE,DIM>& tmp , const Data::Vector& dataV0, int j) {
        for (size_t i = 0; i < DIM; i++) {
        	permpart(tmp.virtDetPoint, i, dataV0[j+i].virtDetPoint);
	}
	// FIXME: make pool configurable
	tmp.emissPnt_idx = rand() % POOLSIZE; 
	tmp.centOfCurv_rad_idx = rand() % POOLSIZE; 
    };

    size_t initlen = dataV1.size();

    for (size_t j = initlen; j < initlen+len; j+=DIM) {   // FIXME: array out of bounds!
            VECTYPE_REDUCED::PhotonReflection<TYPE,DIM> tmp __attribute__((__aligned__(64))) = VECTYPE_REDUCED::PhotonReflection<TYPE,DIM>();
            perm(tmp, dataV0, j);
            dataV1.push_back(tmp);
    }
}

template< class TYPE, std::size_t DIM = 16 >
static void shuffle(const Data::Vector& dataV0 , typename VECTYPE::PhotonReflections<TYPE, DIM>& dataV1) {
    size_t len = dataV0.size();
    // std::cout << len << std::endl;
    // FIXME: Unterschied ob ich lesend oder schreibend springe?
    // FIXME: auto& ?
    auto permpart = [&] (auto& tmpfield, int i, const auto& datafield) {
        tmpfield.x()[i] = datafield.x();
        tmpfield.y()[i] = datafield.y();
        tmpfield.z()[i] = datafield.z();
    };
    auto perm = [=, &dataV0] (VECTYPE::PhotonReflection<TYPE,DIM>& tmp , const Data::Vector& dataV0, int i, int j) {
        permpart(tmp.emissPnt, i, dataV0[j+i].emissPnt);
        permpart(tmp.centOfCurv, i, dataV0[j+i].centOfCurv);
        permpart(tmp.virtDetPoint, i, dataV0[j+i].virtDetPoint);
        tmp.radius[i] = dataV0[j+i].radius;
    };

    size_t initlen = dataV1.size();
    for (size_t j = initlen; j < initlen+len; j+=DIM) {   // FIXME: array out of bounds!
        VECTYPE::PhotonReflection<TYPE,DIM> tmp = VECTYPE::PhotonReflection<TYPE,DIM>(); //  __attribute__((__aligned__(8)));
        for (size_t i = 0; i < DIM; i++) {
            perm(tmp, dataV0, i, j);
        }
        dataV1.push_back(tmp);
    }
}


template< class TYPE >
void solve( Data& data )
{
  qSolver.solve<TYPE>( data.emissPnt, 
                       data.centOfCurv, 
                       data.virtDetPoint,
                       data.radius, 
                       data.sphReflPoint );
}

template< class TYPE >
void solveNewton( Data& data )
{
  qSolverNewton.solve<TYPE>( data.emissPnt, 
                       data.centOfCurv, 
                       data.virtDetPoint,
                       data.radius, 
                       data.sphReflPoint );
}

template< class TYPE, size_t DIM >
void solveNew( VECTYPE::PhotonReflection<TYPE>& data )
{
  qSolverCachelineGCC.solve<TYPE, float, DIM>(data);
}

template< class TYPE >
void solve( Data::Vector & dataV )
{
#ifdef DEBUG_RICH
  std::cout << "Solving Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  //for ( auto& data : dataV ) { solve<TYPE>(data); }
  auto end = dataV.end();
  #pragma omp parallel for
  for (auto it = dataV.begin(); it < end; ++it) {
    solve<TYPE>(*it);
  }
}

template< class TYPE>
void solveNewton(Data::Vector & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  auto end = dataV.end();
  #pragma omp parallel for
  for (auto it = dataV.begin(); it < end; ++it) {
    solveNewton<TYPE>(*it);
  }
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}


template< class TYPE, size_t DIM=16>
void solveNew(  typename VECTYPE::PhotonReflections<TYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(TYPE).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  auto end = dataV.end();
  #pragma omp parallel for
  for (auto it = dataV.begin(); it < end; ++it) {
    solveNew<TYPE, DIM>(*it);
  }
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}

template< class VECT, class STYPE, std::size_t DIM = 16 >
inline void solveCachelineReduced( VECTYPE_REDUCED::PhotonReflection<STYPE,DIM>& data )
{
  qSolverCachelineReduced.solve<VECT, STYPE>( data );
}

template< class VECT, class STYPE, std::size_t DIM = 16 >
inline void solveCacheline( VECTYPE::PhotonReflection<STYPE,DIM>& data )
{
  qSolverCacheline.solve<VECT, STYPE>( data );
}

template< class VECT, class STYPE, std::size_t DIM = 16>
void solveCachelineReduced( typename VECTYPE_REDUCED::PhotonReflections<STYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving reduced data Quartic Equation for "
            << typeid(VECT).name()
            << " Photons with " << nchunks
	    << " ..." << std::endl;
#endif /* DEBUG_RICH */
  const auto end = dataV.end();
  const auto begin= dataV.begin();
  INSERT_SSC_MARK(0x33333333);
#pragma omp parallel for schedule(static,nchunks)
  for (auto it = begin; it < end; it++) {
	solveCachelineReduced<VECT, STYPE, DIM>(*it);
  }
  INSERT_SSC_MARK(0x44444444);
}

template< class VECT, class STYPE, std::size_t DIM = 16>
void solveCacheline( typename VECTYPE::PhotonReflections<STYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(VECT).name()
            << " Photons with " << nchunks
	    << " ..." << std::endl;
#endif /* DEBUG_RICH */
  const auto end = dataV.end();
  const auto begin= dataV.begin();
  //default(none) shared(dataV)
  INSERT_SSC_MARK(0x11111111);
#pragma omp parallel for schedule(static,nchunks)
  for (auto it = begin; it < end; it++) {
	solveCacheline<VECT, STYPE, DIM>(*it);
  }
  INSERT_SSC_MARK(0x22222222);
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}
template< class VECT, class STYPE, std::size_t DIM = 16 >
inline void solveCachelineGCC( VECTYPE::PhotonReflection<STYPE,DIM>& data )
{
  qSolverCachelineGCC.solve<VECT, STYPE, 16>( data ); 
}

template< class VECT, class STYPE, std::size_t DIM = 16>
void solveCachelineGCC( typename VECTYPE::PhotonReflections<STYPE, DIM> & dataV )
{
#ifdef DEBUG_RICH
    std::cout << "Solving (new) Quartic Equation for "
            << typeid(VECT).name()
            << " Photons ..." << std::endl;
#endif /* DEBUG_RICH */
  auto end = dataV.end();
  //default(none) shared(dataV)
#pragma omp parallel for schedule(static)
  for (auto it = dataV.begin(); it < end; ++it) {
    solveCachelineGCC<float, float, DIM>(*it);
  }
  // for ( const auto& data : dataV ) { solveNew<TYPE>(data); }
}


template<class T,  void (*F) (T&) >
static void benchmark(T& dataV,const std::string funcname )
{
    uint16_t i = 0;
    const auto start = std::chrono::high_resolution_clock::now();
    for(i=0;i<nIterations;i++)
        //solveCacheline<Vec16f, float>(dataV);
        F(dataV);
    const auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::nano> diff = end - start;
    auto avg = diff.count()/static_cast<double>(nPhotons*nIterations);
    std::cout << funcname << ", " << avg << std::endl;
}

int main ( int argc, char** argv)
{
  //const unsigned int nPhotons = 1e6;
  //const unsigned int nPhotons = 1e3;
  int ret = 0;

  if (argc > 1) {
      //FIXME: 64 bit? or default int?
    nPhotons = atol(argv[1]);
  }

  if (argc > 2) {
    nIterations = atol(argv[2]);
  }

  if (argc > 3) {
    nchunks = atol(argv[3]);
  }


  Data::Vector dataV[NRANDSEED];
  Data::Vector dataV0;
  Data::Vector dataV1;
  Data::Vector dataV2;
  VECTYPE::PhotonReflections<float> dataV0_vect  __attribute__((__aligned__(64)));
  VECTYPE::PhotonReflections<float> dataV1_vect  __attribute__((__aligned__(64)));
  VECTYPE_REDUCED::PhotonReflections<float> dataV0_red_vect  __attribute__((__aligned__(64)));

  // Construct the data to work on
  std::cout << "Creating " << nPhotons << " random photons ..." << std::endl;
  auto photonbatch = nPhotons/NRANDSEED;
#pragma omp parallel for
  for ( unsigned int j = 0 ; j < NRANDSEED; j++) {
    dataV[j].reserve( photonbatch );
    for ( unsigned int i = 0; i < photonbatch; i++) {
        dataV[j].push_back( Data(j*i) );
    }
  }
  /*
  for ( unsigned int i = 0; i < nPhotons; ++i ) {
      dataV0[i]=Data();
  }
  */

  // FIXME: 16 and 8 already included in data type


  dataV0.reserve( nPhotons );
  for (size_t i = 0; i < NRANDSEED; i++) {
      dataV0.insert(dataV0.end(), dataV[i].begin(), dataV[i].end());
      dataV[i].clear();
      dataV[i].shrink_to_fit();
  }
#ifdef SKALAR
  dataV1 = dataV0;
  // run the solver for floats
  benchmark<Data::Vector&, solve<float>>(dataV1, std::string("solve<float>"));
#ifdef DEBUG_RICH
  std::cout << "First reflection point: " << dataV1[0].sphReflPoint << std::endl;
  std::cout << "First reflection point: " << dataV1[15].sphReflPoint << std::endl;
  std::cout << "First reflection point: " << dataV1[16].sphReflPoint << std::endl;
#endif
  // run the solver for doubles
//  benchmark<Data::Vector&, solve<double>>(dataV1, std::string("solve<double>"));
  dataV1.clear();
  dataV1.shrink_to_fit();
/*
  // run the solver for doubles
  benchmark(solveNew<double>, dataV3, "solveNew<double>" );
*/
#endif /* SKALAR */
  // run the solver for floats (vectorclass)
  shuffle<float,16>(dataV0, dataV0_vect);
 // dataV0.clear();
  //dataV0.shrink_to_fit();
  // run the solver for floats
  //shuffle<double,8>(dataV0, dataV1_vect);
// #ifdef VTUNE
//   __itt_resume();
// #endif //VTUNE
  benchmark<VECTYPE::PhotonReflections<float>&, solveCacheline<Vec16f, float>>(dataV0_vect, std::string("solveCacheline<Vec16f>"));
// #ifdef VTUNE
//   __itt_pause();
// #endif //VTUNE
  // benchmark<typeof(dataV0_vect), decltype<solveCacheline>> (solveCacheline, dataV0_vect, "solveCachelineline<float>" );
  
  shuffle_reduced<float,16>(dataV0, dataV0_red_vect);
  benchmark<VECTYPE_REDUCED::PhotonReflections<float>&, solveCachelineReduced<Vec16f, float>>(dataV0_red_vect, std::string("solveCachelineReduced<Vec16f>"));

#ifdef GCCAUTO
  shuffle<float,16>(dataV0, dataV1_vect);
  benchmark<VECTYPE::PhotonReflections<float>&, solveCachelineGCC<Vec16f, float>>(dataV1_vect, std::string("solveCachelineGCC<float>"));
#ifdef DEBUG_RICH
  std::cout << "First reflection point GCCauto_vec: \n" << dataV1_vect[0].sphReflPoint << std::endl;
  std::cout << "First reflection point GCCauto_vec: \n" << dataV1_vect[15].sphReflPoint << std::endl;
  std::cout << "First reflection point GCCauto_vec: \n" << dataV1_vect[16].sphReflPoint << std::endl;
#endif /* DEBUG_RICH */
#endif /* GCCAUTO */

#ifdef DEBUG_RICH
  std::cout << "First reflection point Vec16f: \n" << dataV0_vect[0].sphReflPoint << std::endl;
#endif
#ifdef TIMERS
  std::cout << "Timers (" << nPhotons << "):" << std::endl;
  std::cout << "transform: " << timer_transform.count()/1.0e9 << std::endl;
  std::cout << "quartic: " << timer_quartic.count()/1.0e9 << std::endl;
  std::cout << "cbrt: " << timer_cbrt.count()/1.0e9 << std::endl;
#endif /* TIMERS */

#ifdef SKALAR
  dataV2 = dataV0;
  benchmark<Data::Vector&, solveNewton<float>> (dataV2, std::string("solveNewton<float>" ));
#ifdef DEBUG_RICH
  std::cout << "First reflection point: " << dataV2[0].sphReflPoint << std::endl;
#endif
  dataV2.clear();
  dataV2.shrink_to_fit();
#endif	/* SKALAR */

  /*
  benchmark<VECTYPE::PhotonReflections<double,8>&, solveCacheline<Vec8d, double, 8>>(dataV1_vect, std::string("solveCacheline<Vec8d>"));
  std::cout << "1. reflection point: " << dataV1_vect[1].sphReflPoint << std::endl;
  std::cout << "9. reflection point: " << dataV1_vect[7].sphReflPoint << std::endl;
*/
  // run the solver for doubles (vectorclass)
  // FIXME: implement double version
  // benchmark(solveCachelineline<Vec8d>, dataV5, "solveCachelineline<double>" );
  // std::cout << "First reflection point: " << dataV3[0].sphReflPoint << std::endl;

  return ret;
}

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import json

class Platform:
    def __init__(self, name, perf_peaks, bws): 
        self.name = name
        self.perf_peaks = perf_peaks
        self.bandwidths = bws
        
    def get_mem_hierarchy(self):
        bandwidths = self.bandwidths.items()
        bandwidths.sort(cmp=lambda e1, e2: cmp(e1[1], e2[1]))
        return bandwidths
    
    def get_perf_roofs(self):
        perfs = self.perf_peaks.items()
        perfs.sort(cmp=lambda e1, e2: cmp(e1[1], e2[1]))
        return perfs
    
    @classmethod
    def from_ert_json(cls, name, ert_json_str):
        ert_res = json.loads(json_str)
        bandwidths = {n: bw for n, bw in ert_res['empirical']['gbytes']['data']}
        perf = ert_res['empirical']['gflops']['data'][0][1]
        return cls(name, {'Peak Performance': perf}, bandwidths)

        
def calculate_mem_roof(bw, perf, opint_lims=[-5, 4]):
    """Calculates a bandwidth roofline
    
    bw -- The bandwidth, can be the max bandwidth or a synthetic bound
    perf -- the performance until which the bound should be calculated
    opint_lims -- operation intensity limits in base 2 powers
    """
    mem_roof_int = np.logspace(opint_lims[0],opint_lims[1],200,base=2)
    mem_roof = mem_roof_int * bw
    mem_roof_int, mem_roof = zip(*filter(lambda mem: mem[1] <= perf,
                                          zip(mem_roof_int,mem_roof)))
    return mem_roof_int, mem_roof
        
def calculate_mem_roofs(platform, opint_lims=[-5, 4]):
    bws = platform.get_mem_hierarchy()
    max_perf = platform.get_perf_roofs()[-1]
    roofs = []
    for name, bw in bws:
        roofs.append((name, bw, calculate_mem_roof(bw, max_perf[1], opint_lims)))
    return roofs

def label_orientation(line, p1, p2):
    ax = line.get_axes()
    #ax.set_xscale('log', basex=2)
    #ax.set_yscale('log', basey=2)
    xp1 = ax.transData.transform_point(p1)
    xp2 = ax.transData.transform_point(p2)
    x = xp2[0] - xp1[0]
    y = xp2[1] - xp1[1]
    deg = np.degrees(np.arctan(y/x))
    return deg

def roof_here(max_bw, perf_roofs,arith_int):
    max_perf = max(perf_roofs)
    return min(arith_int*max_bw,max_perf)

def roofline_plot(platform, perf_data, opint_lims=[-5,4], perf_lims=[-2,6], **kwargs):
    """Creates a roofline plot
    
    platform -- an instance of Platform describing the hardware parameters
    perf_data -- a dictionary of performance measurements {'name':(op.intensity,perf),...}
    opint_lims -- operational intensity limits in powers of base 2 (used for axis limits)
    perf_lims -- performance limits in powers of base 2 (used for axis limits)
    """
    
    # calculate the roofs
    mem_roofs = calculate_mem_roofs(platform, opint_lims=opint_lims)
    mem_rooflines = []
    for name, bw, roof in mem_roofs:
        line, = plt.plot(roof[0], roof[1], ':k')
        mem_rooflines.append((name, bw, roof, line))
    
    perf_roof_names, perf_roofs = zip(*platform.get_perf_roofs())
    
    max_bw = platform.get_mem_hierarchy()[-1][1]
    perf_roof_starts = [perf/max_bw for perf in perf_roofs]
    
    # draw the roofs
    plt.hlines(perf_roofs, perf_roof_starts, 
               xmax=2**opint_lims[1],linestyles=':',colors='k')
       
    for name, flop in platform.get_perf_roofs():
        tl = plt.text(2**opint_lims[1], flop, '%s: %.2f GFLOP/s'%(name,flop),
            horizontalalignment='right',verticalalignment='center',fontsize=kwargs['fontsize'])
        tl.set_bbox(dict(color='white', alpha=0.8))
    
    # draw axis, labels, etc.
    plt.xscale('log',basex=2)
    plt.yscale('log',basey=2)
    loc, _ = plt.xticks()
    plt.xticks(loc,['%5.3f'%x for x in loc])
    loc, _ = plt.yticks()
    plt.yticks(loc,['%5.3f'%x for x in loc])
    plt.xlim([2**l for l in opint_lims])
    plt.ylim([2**l for l in perf_lims])
    
    # put roof label
    for name, bw, roof, line in mem_rooflines:
        deg = label_orientation(line, (roof[0][2], roof[1][2]), (roof[0][5], roof[1][5]))
        tl = plt.text(roof[0][1],roof[1][2],
                '%s BW: %.2f GB/s'%(name,bw),rotation=deg,
                horizontalalignment='left',verticalalignment='bottom',fontsize=kwargs['fontsize'])
    # plot arithmetic intensities
    plt.vlines([opint for _, (opint, _) in perf_data.iteritems()],2**perf_lims[0],
               [roof_here(max_bw,perf_roofs,opint) for _, (opint, _) in perf_data.iteritems()],
              linestyles='--',colors='gray')    
    # plot the performance measurements
    for label, (opint, perf) in perf_data.iteritems():
        plt.plot(opint, perf,'o',label=label)
    if 'fontsize' in kwargs:
        plt.xlabel('Arithmetic intensity [FLOP/Byte]',fontsize=kwargs['fontsize'])
        plt.ylabel('Performance GFLOP/s', fontsize=kwargs['fontsize'])
    else:
        plt.xlabel('Arithmetic intensity [FLOP/Byte]')
        plt.ylabel('Performance GFLOP/s')
    handles, labels = plt.gca().get_legend_handles_labels()
    # sort both labels and handles by labels
    if len(labels) > 0:
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: -1*len(t[0])))
        plt.legend(handles, labels, loc=2)
    #plt.legend(loc=2)
    if 'title' in kwargs:
        plt.title(kwargs['title'])
    else:
        plt.title(platform.name)
    plt.show()
    return plt.gcf()

def main():
    # example usage
#    dp_sca_peak = 3.44
    sp_fma_peak = 6E3
    dp_fma_peak = 3E3
    sp_add_peak = 487
#    dp_fma_peak = 31.79
    
    dram_bw = 80
    mcdram_bw = 340
    l2_bw = 1827
    l1_bw = 6040
    
    hsw_2683v3_1core = Platform('KNL 7210', 
                                {#'DP scalar peak': dp_sca_peak,
                                 'SP add peak': sp_add_peak,
                                 'SP FMA peak': sp_fma_peak,
                                 'DP FMA peak': dp_fma_peak,
                                 #'DP FMA peak': dp_fma_peak
                                 }, 
                                {'DDR RAM': dram_bw, 
                                 'L1': l1_bw, 
                                 'L2': l2_bw, 
                                 'MCDRAM': mcdram_bw})
    
    haswell_vp_perfs = {}
#    haswell_vp_perfs['FindBestFit'] = (0.594, 15.731)
    haswell_vp_perfs['nphotons=615K(meas.)'] = (4, 6E3)
    haswell_vp_perfs['nphotons=615K(theor.)'] = (4, 100)
    rp = roofline_plot(hsw_2683v3_1core, haswell_vp_perfs, opint_lims=[-5,8], perf_lims=[-2,13], fontsize=12)
    rp.savefig('velopix_haswell.pdf',bbox_inches='tight')

if __name__ == "__main__":
    main()

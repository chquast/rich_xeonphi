#include "gtest/gtest.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#include "QuarticSolverCachelineReduced.h"
#include "QuarticSolver.h"
#include "vectype_reduced.h"

#pragma GCC diagnostic pop

#define MAXDIFF 0.0001

TEST(SolveQuarticReducedDataTest, Vec16fFullQuarticReducedDataSolv) {
    using  T = float;
    const std::size_t DIM = 16;

    VECTYPE_REDUCED::PhotonReflection<T, DIM> pr;
    pr.emissPnt_idx = 1;
    pr.virtDetPoint.x() = {-1060.06, 424.919, 2959.81, -1503.66, 344.973, -1244.87, -681.211, -64.3068, -1601.12, 31.6497, 637.536, -2744.88, 2302.78, -2470.78, -1224.58, -2280.8, };
    pr.virtDetPoint.y() = {-101.037, 166.176, 18.4349, 54.9742, 146.207, -149.939, -65.3358, 35.7236, 185.953, 17.1518, -139.488, 185.495, 151.719, 188.42, -9.88739, -103.121, };
    pr.virtDetPoint.z() = {8145.2, 8102.3, 8106.47, 8102.97, 8132, 8144.69, 8107.63, 8195.64, 8187.03, 8136.91, 8108.54, 8145.96, 8146.85, 8177.47, 8185.05, 8123.23, };
    pr.centOfCurv_rad_idx = 2;

    typedef typename RichCachelineReducedData::Rec::QuarticSolverCacheline<T>::XYZPoint XYZPoint;
    std::vector<XYZPoint>  EmissPntPool{{-485.943, -284.033, 10262.3}, {62.415, 284.423, 10267.2}};
    std::vector<T>  RadiusPool{ 8510.35, 8576.92, 8543.37 };
    std::vector<XYZPoint>  CoCPool{{1892.93, -9.71593, 3351.23}, {-1599.15, -1.12108, 3386.25}, {-1024.33, -13.692, 3305.28}};


    RichCachelineReducedData::Rec::QuarticSolverCacheline<float> qSolverCachelineReduced(EmissPntPool, RadiusPool, CoCPool);

    Rich::Rec::QuarticSolver qSolver;
    qSolverCachelineReduced.solve<Vec16f, float>(pr);
    constexpr auto vecsize = 16;

    for(int i = 0; i < vecsize; i++) {
	auto epoint = EmissPntPool[pr.emissPnt_idx];
        const Gaudi::XYZPoint emissPnt = {epoint[0], epoint[1], epoint[2]};
	auto cpoint = CoCPool[pr.centOfCurv_rad_idx];
        const Gaudi::XYZPoint centOfCurv = {cpoint[0], cpoint[1], cpoint[2]};
        const Gaudi::XYZPoint virtDetPoint = {pr.virtDetPoint.x()[i],
            pr.virtDetPoint.y()[i], pr.virtDetPoint.z()[i]};
        Gaudi::XYZPoint sphReflPoint;

        qSolver.solve<double>(emissPnt, centOfCurv, virtDetPoint, RadiusPool[pr.centOfCurv_rad_idx], sphReflPoint);
        // EXPECT_FLOAT_EQ(static_cast<double>(sphReflPoint.x()), pr.sphReflPoint.x()[i]);
        // EXPECT_FLOAT_EQ(static_cast<double>(sphReflPoint.y()), pr.sphReflPoint.y()[i]);
        // EXPECT_FLOAT_EQ(static_cast<double>(sphReflPoint.z()), pr.sphReflPoint.z()[i]);
        EXPECT_NEAR(static_cast<double>(sphReflPoint.x()), pr.sphReflPoint.x()[i], abs(sphReflPoint.x()*MAXDIFF));
         EXPECT_NEAR(static_cast<double>(sphReflPoint.y()), pr.sphReflPoint.y()[i], abs(sphReflPoint.y()*MAXDIFF));
        EXPECT_NEAR(static_cast<double>(sphReflPoint.z()), pr.sphReflPoint.z()[i], abs(sphReflPoint.z()*MAXDIFF));
    }
}


//----------------------------------------------------------------------
/** @file QuarticSolverCacheline.h
 *
 *  @author Christina Quast	christina.quast@cern.ch
 *  @date   2017-09-29
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-01-27
 */
//----------------------------------------------------------------------

#ifndef RICHRECPHOTONTOOLS_QuarticSolverCacheline_H
#define RICHRECPHOTONTOOLS_QuarticSolverCacheline_H

#if not defined STL
// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#else

// STL
#include <type_traits>

#endif

// VectorClass
#include "vectorclass512.h"
#include "VectorClass/complexvec.h"
#include "VectorClass/vectormath_trig.h"
#include "VectorClass/vectormath_exp.h"

#include "vectype.h"

// Eigen
#include "LHCbMath/EigenTypes.h"
#include <Eigen/Geometry>

// LHCb Maths
#include "LHCbMath/FastRoots.h"
#include <tuple>

#include "debug.h"

namespace RichCacheline
{
    namespace Rec
    {

        //-----------------------------------------------------------------------------
        /** @class QuarticSolverCacheline
         *
         *  Utility class that implements the solving of the Quartic equation for the RICH
         *
         *  @author Chris Jones         Christopher.Rob.Jones@cern.ch
         *  @date   2015-01-27
         */
        //-----------------------------------------------------------------------------
        class QuarticSolverCacheline
        {

            public:

                // Use eigen types
                typedef LHCb::Math::Eigen::XYZPoint  Point;   ///< Point type
                typedef LHCb::Math::Eigen::XYZVector Vector;  ///< vector type

            public:
                // FIXME: performance penalty becaus of tuple?
                template <typename T>
                inline std::tuple<T, T, T> transform(T evecX, T evecY, T evecZ, T dvecX, T dvecY, T dvecZ,
                                            T CoCX, T CoCY, T CoCZ, T sinbeta, T radius, T e) const {
                        T nx = (evecY*dvecZ) - (evecZ*dvecY);
                        T ny = (evecZ*dvecX) - (evecX*dvecZ);
                        T nz = (evecX*dvecY) - (evecY*dvecX);

                        const T norm = (nx*nx+ny*ny+nz*nz);
                        T norm_sqrt = approx_recipr(approx_rsqrt(norm));

                        // FIXME: is there a more performant asin function?
                        // FIXME: where else to add const?
                        // FIXME: alignment
                        // auto beta = asin_f(sinbeta);
                        const auto a = sinbeta*norm_sqrt;
                        INSERT_SSC_MARK(0x55555555);
                        const auto b = (1.0f-approx_recipr(approx_rsqrt(1.0f-(sinbeta*sinbeta))));
                        INSERT_SSC_MARK(0x66666666);
                        const auto enorm = radius*approx_recipr(e*norm);
                        // symmetric matrix, diagonal part is 0

                        const std::array<T, 9> M = {norm+b*(-nz*nz-ny*ny), a*nz+b*nx*ny, -a*ny+b*nx*nz,
                                                    -a*nz+b*nx*ny, norm+b*(-nx*nx-nz*nz), a*nx+b*ny*nz,
                                                    a*ny+b*nx*nz, -a*nx+b*ny*nz, norm+b*(-ny*ny-nx*nx)};


                        const auto ex = enorm*(evecX*M[0]+evecY*M[3]+evecZ*M[6]);
                        const auto ey = enorm*(evecX*M[1]+evecY*M[4]+evecZ*M[7]);
                        const auto ez = enorm*(evecX*M[2]+evecY*M[5]+evecZ*M[8]);

                        const T reflPointX = ex + CoCX;
                        const T reflPointY = ey + CoCY;
                        const T reflPointZ = ez + CoCZ;
                        return std::tuple<T, T, T>(reflPointX, reflPointY, reflPointZ);
                }

                /** Solves the characteristic quartic equation for the RICH optical system.
                 *
                 *  See note LHCB/98-040 RICH section 3 for more details
                 *
                 *  @param emissionPoint Assumed photon emission point on track
                 *  @param CoC           Spherical mirror centre of curvature
                 *  @param virtDetPoint  Virtual detection point
                 *  @param radius        Spherical mirror radius of curvature
                 *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
                 *
                 *  @return boolean indicating status of the quartic solution
                 *  @retval true  Calculation was successful. sphReflPoint is valid.
                 *  @retval false Calculation failed. sphReflPoint is not valid.
                 */
                template< class VECT, class SKALART >
                    inline void solve( VECTYPE::PhotonReflection<SKALART>& data) const
                    // const VECTYPE& emissionPoint,
                    // const VECTTYPE& CoC,
                    // const VECTYPE& virtDetPoint,
                    // const VECTTYPE radius[],
                    // VECTTYPE& sphReflPoint ) const
                    {
			__builtin_prefetch(&(((&data)+0)->radius[0]), 0, 3);

			__builtin_prefetch(&(((&data+1)->emissPnt.x())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->emissPnt.y())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->emissPnt.z())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->centOfCurv.x())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->centOfCurv.y())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->centOfCurv.z())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->virtDetPoint.x())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->virtDetPoint.y())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->virtDetPoint.z())[0]), 0, 3);

                        // TODO :align 64
                        // FIXME: ueberall const dranmachen?
                        VECT emissionPointVecX;
                        emissionPointVecX.load_a(&data.emissPnt.x()[0]);
                        VECT emissionPointVecY;
                        emissionPointVecY.load_a(&data.emissPnt.y()[0]);
                        VECT emissionPointVecZ;
                        emissionPointVecZ.load_a(&data.emissPnt.z()[0]);

                        VECT CoCX;
                        CoCX.load_a(&data.centOfCurv.x()[0]);
                        VECT CoCY;
                        CoCY.load_a(&data.centOfCurv.y()[0]);
                        VECT CoCZ;
                        CoCZ.load_a(&data.centOfCurv.z()[0]);

                        //const Vector evec( emissionPoint - CoC );
                        const VECT evecX = emissionPointVecX - CoCX;
                        const VECT evecY = emissionPointVecY - CoCY;
                        const VECT evecZ = emissionPointVecZ - CoCZ;

                        // vector from mirror centre of curvature to assumed emission point
                        // const TYPE e2 = evec.dot(evec);
                        const VECT e2 = evecX*evecX + evecY*evecY + evecZ*evecZ;

                        // vector from mirror centre of curvature to virtual detection point
                        VECT virtDetPointVecX;
                        virtDetPointVecX.load_a(&data.virtDetPoint.x()[0]);
                        VECT virtDetPointVecY;
                        virtDetPointVecY.load_a(&data.virtDetPoint.y()[0]);
                        VECT virtDetPointVecZ;
                        virtDetPointVecZ.load_a(&data.virtDetPoint.z()[0]);

                        // const Vector dvec( virtDetPoint - CoC );
                        const VECT dvecX = virtDetPointVecX - CoCX;
                        const VECT dvecY = virtDetPointVecY - CoCY;
                        const VECT dvecZ = virtDetPointVecZ - CoCZ;

                        // const TYPE d2 = dvec.dot(dvec);
                        const VECT d2 = dvecX*dvecX + dvecY*dvecY + dvecZ*dvecZ;

                        // various quantities needed to create quartic equation
                        // see LHCB/98-040 section 3, equation 3
                        //const auto ed2 = e2 * d2;
                        const VECT ed2 = e2 * d2;
                        const VECT evecDvec = evecX*dvecX + evecY*dvecY + evecZ*dvecZ;


                        // TODO: compare at this point
                        // TODO: div aufwändig? durch * 1/ed2 ersetzen?
                        // const TYPE cosgamma2 = ( ed2 > 0 ? std::pow(evec.dot(dvec),2)/ed2 : 1.0 );
                        // FIXME: durch square = pow of 2 ersetzen?
                        // approx_recipr(a); 
                        VECT cosgamma2 = (evecDvec * evecDvec)*approx_recipr(ed2);
                        cosgamma2 = select(ed2 > 0, cosgamma2, 1.0f);

                        // vectorise 4 square roots into 1
                        //using Vec4x = 
                        //  typename std::conditional<std::is_same<TYPE,float>::value,Vec4f,Vec4d>::type;
                        // const auto tmp_sqrt = sqrt( Vec4x( e2, d2,
                        //                                   cosgamma2 < 1.0 ? 1.0-cosgamma2 : 0.0,
                        //                                   cosgamma2 ) );
                        // const auto e         = tmp_sqrt[0];
                        // const auto d         = tmp_sqrt[1];
                        // const auto singamma  = tmp_sqrt[2];
                        // const auto cosgamma  = tmp_sqrt[3];

                        const VECT e = approx_recipr(approx_rsqrt(e2));
                        const VECT d = approx_recipr(approx_rsqrt(d2));

                        const VECT singamma = approx_recipr(approx_rsqrt(1.0f - cosgamma2));  // TODO: kann es echt > 1.0 werden?
                        const VECT cosgamma = approx_recipr(approx_rsqrt(cosgamma2));

                        // const auto dx        = d * cosgamma;
                        // const auto dy        = d * singamma;
                        // const auto r2        = radius * radius;
                        // const auto dy2       = dy * dy;
                        // const auto edx       = e + dx;
                        const VECT dx = d * cosgamma;
                        const VECT dy = d * singamma;
                        VECT radius;
                        radius.load_a(&data.radius[0]);
                        const VECT r2 = radius * radius;
                        const VECT dy2 = dy * dy;
                        const VECT edx = e + dx;

                        const VECT a0 = 4.0f * ed2;
                        const VECT maxval  =  std::numeric_limits<SKALART>::max();
                        const VECT dyrad2    = 2.0f * dy *radius;
                        const VECT a1        = -( 2.0f * dyrad2 * e2 );
                        const VECT a2        = ( ( dy2* r2 ) + (edx *edx * r2) - a0 );
                        const VECT a3        = ( (dyrad2 * e * (e-dx)));
                        const VECT a4        = ( (e2 - r2) * dy2 );

                        // use simplified RICH version of quartic solver
                        TIMER_START(timer_quartic);
			//Use optimized newton solver on quartic equation.
			const auto sinbeta = solve_quartic_newton_RICH( a0, a1, a2, a3, a4 );
			//TODO: This method should be better but has problems still for some reasons
			//const auto sinbeta = solve_quartic_housholder_RICH<TYPE, 3>(a1, a2, a3, a4);
			// construct rotation transformation
			// Set vector magnitude to radius
			// rotate vector and update reflection point
			//rotation matrix uses sin(beta) and cos(beta) to perform rotation
			//even fast_asinf (which is only single precision and defeats the purpose
			//of this class being templatable to double btw) is still too slow
			//plus there is a cos and sin call inside AngleAxis ...
			//We can do much better by just using the cos(beta) we already have to calculate
			//sin(beta) and do our own rotation. On top of that we rotate non-normalized and save several
			//Divisions by normalizing only once at the very end
			//Again, care has to be taken since we are close to float_max here without immediate normalization.
					TIMER_STOP(timer_quartic);
                        PR0(sinbeta);

                        VECT reflPointX  __attribute__((__aligned__(64)));
                        VECT reflPointY  __attribute__((__aligned__(64)));
                        VECT reflPointZ __attribute__((__aligned__(64)));

                        // FIXME: pass by value or ref?

                        TIMER_START(timer_transform);
                        std::tuple<VECT, VECT, VECT> reflp =  transform(evecX, evecY, evecZ,
                                                                            dvecX, dvecY, dvecZ,
                                                                            CoCX, CoCY, CoCZ,
                                                                            sinbeta, radius, e);
                        TIMER_STOP(timer_transform);

                        std::tie (reflPointX, reflPointY, reflPointZ) = reflp;
                        // (normalised) normal vector to reflection plane
                        // auto n = evec.cross3(dvec);
                        //   n /= std::sqrt( n.dot(n) );

                        /*
                        // construct rotation transformation
                        // Set vector magnitude to radius
                        // rotate vector and update reflection point
                        typedef Eigen::Matrix< TYPE , 3 , 1 > Eigen3Vector;
                        const Eigen::AngleAxis<TYPE> angleaxis( vdt::fast_asinf(sinbeta),
                        Eigen3Vector(n[0],n[1],n[2]) );
                        sphReflPoint = ( CoC + Gaudi::XYZVector( angleaxis *
                        Eigen3Vector(evec[0],evec[1],evec[2]) *
                        ( radius / e ) ) );
                        */

		    	__builtin_prefetch(&data.sphReflPoint.x()[0], 1, 0);
		    	__builtin_prefetch(&data.sphReflPoint.y()[0], 1, 0);
		    	__builtin_prefetch(&data.sphReflPoint.z()[0], 1, 0);
                        reflPointX.store_a(&data.sphReflPoint.x()[0]);
                        reflPointY.store_a(&data.sphReflPoint.y()[0]);
                        reflPointZ.store_a(&data.sphReflPoint.z()[0]);
                    }

	private:
	      //A newton iteration solver for the Rich quartic equation
	      //Since the polynomial that is evaluated here is extremely constrained
	      //(root is in small interval, one root guaranteed), we can use a much more
	      //efficient approximation (which still has the same precision) instead of the
	      //full blown mathematically absolute correct method and still end up with
	      //usable results
	      template < class TYPE >
		    inline TYPE f4(const TYPE &a0, const TYPE &a1, const TYPE &a2, const TYPE &a3, const TYPE &a4, TYPE &x) const
		    {
			//return (a0 * x*x*x*x + a1 * x*x*x + a2 * x*x + a3 * x + a4);
			//A bit more FMA friendly
			return ((((a0*x)+a1)*x+a2)*x+a3)*x+a4;
		    }
		template < class TYPE >
		    inline TYPE df4(const TYPE &a0, const TYPE &a1, const TYPE &a2, const TYPE &a3, TYPE &x) const
		    {
			//return (4.0f*a0 * x*x*x + 3.0f*a1 * x*x + 2.0f*a2 * x + a3);
			return (((4.0f * a0*x)+3.0f*a1)*x+2.0f*a2)*x+a3;
		    }
		      /** Horner's method to evaluate the polynomial and its derivatives with as little math operations as
		       *  possible. We use a template here to allow the compiler to unroll the for loops and produce code
		       *  that is free from branches and optimized for the grade of polynomial and derivatives as necessary.
		       */
		template < class TYPE, std::size_t ORDER = 4, std::size_t DIFFGRADE = 3 >
		    inline void evalPolyHorner(const TYPE (&a)[ORDER+1], TYPE (&res)[DIFFGRADE+1], TYPE x) const
		    {
			for(unsigned int i = 0; i <= DIFFGRADE; i++)
			{
			  res[i] = a[0];
			}
			for(unsigned int j = 1; j <= ORDER; j++)
			{
			  res[0] = res[0] * x + a[j];
			  int l = (ORDER - j) > DIFFGRADE ? DIFFGRADE : ORDER - j;
			  for (int i = 1; i <= l ; i++)
			  {
			    res[i] = res[i] * x + res[i-1];
			  }
			}
			//TODO: Check assembly to see if this is optimized away if DIFFGRADE is <= 2
			float l = 1.0;
			for(unsigned int i = 2; i <= DIFFGRADE; i++)
			{
			  l *= i;
			  res[i] = res[i] * l;
			}
		    }
		      /** Newton-Rhapson method for calculating the root of the rich polynomial. It uses the bisection method in the beginning
		       *  to get close enough to the root to allow the second stage newton method to converge faster. After 4 iterations of newton
		       *  precision is as good as single precision floating point will get you. We have introduced a few tuning parameters like the
		       *  newton gain factor and a slightly skewed bisection division, which in this particular case help to speed things up.
		       *  TODO: Once we are happy with the number of newton and bisection iterations, this function should be templated to the number of
		       *  these iterations to allow loop unrolling and elimination of unnecessary branching
		       *  TODO: These tuning parameters have been found by low effort experimentation on random input data. A more detailed
		       *  study should be done with real data to find the best values
		       */
		template < class TYPE >
		    inline TYPE solve_quartic_newton_RICH( const TYPE& a0,
							       const TYPE& a1,
							       const TYPE& a2,
							       const TYPE& a3,
							       const TYPE& a4 ) const
		    {
			TYPE epsilon;
			//Use N steps of bisection method to find starting point for newton
			TYPE l(0);
			TYPE u(0.5);
			TYPE m(0.2); //We start a bit off center since the distribution of roots tends to be more to the left side
			const TYPE a[5] = {a0, a1, a2, a3, a4};
			TYPE res[2];
			for ( int i = 0; i < 3; ++i )
			{
			  auto oppositeSign = sign_bit(f4(a0,a1,a2,a3,a4,m) * f4(a0,a1,a2,a3,a4,l));
			  l = select(oppositeSign, l, m);
			  u = select(oppositeSign, m, u);
			  //std::cout << l.extract(0) << ", " << m.extract(0) << ", " << u.extract(0) << ", " << oppositeSign.extract(0) << std::endl;
			  //0.4 instead of 0.5 to speed up convergence. Most roots seem to be closer to 0 than to the extreme end
			  m = (u + l) * 0.4;
			}
			//Newton for the rest
			TYPE x = m;
			//Most of the times we are approaching the root of the polynomial from one side
			//and fall short by a certain fraction. This fraction seems to be around 1.04 of the
			//quotient which is subtracted from x. By scaling it up, we take bigger steps towards
			//the root and thus converge faster.
			//TODO: study this factor more closely it's pure guesswork right now. We might get
			//away with 3 iterations if we can find an exact value
			const TYPE gain = 1.04;
			for ( int i = 0; i < 4; ++i )
			{
			  evalPolyHorner<TYPE, 4, 1>(a, res, x);
			  //epsilon = f4(a0,a1,a2,a3,a4,x) / df4(a0,a1,a2,a3,x);
			  epsilon = res[0] / res[1];
			  x = x - gain * epsilon;
			}
			return x;
		    }
        };

    }
}

#endif // RICHRECPHOTONTOOLS_QuarticSolverCacheline_H

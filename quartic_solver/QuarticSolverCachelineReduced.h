
//----------------------------------------------------------------------
/** @file QuarticSolverNew.h
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-01-27
 */
//----------------------------------------------------------------------

#ifndef RICHRECPHOTONTOOLS_QuarticSolverCachelineReduced_H
#define RICHRECPHOTONTOOLS_QuarticSolverCachelineReduced_H

#if not defined STL
// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#else

// STL
//#include <math.h>
#include <type_traits>
//#include <complex>

#endif

// VectorClass
#include "vectorclass512.h"
#include "VectorClass/complexvec.h"
#include "VectorClass/vectormath_trig.h"
#include "VectorClass/vectormath_exp.h"

#include "vectype_reduced.h"

// Eigen
#include "LHCbMath/EigenTypes.h"
#include <Eigen/Geometry>

// VDT
//#include "vdt/asin.h"

// LHCb Maths
#include "LHCbMath/FastRoots.h"
#include <tuple>

#include "debug.h"

namespace RichCachelineReducedData
{
    namespace Rec
    {

        //-----------------------------------------------------------------------------
        /** @class QuarticSolverCacheline
         *
         *  Utility class that implements the solving of the Quartic equation for the RICH
         *
         *  @author Chris Jones         Christopher.Rob.Jones@cern.ch
         *  @date   2015-01-27
         */
        //-----------------------------------------------------------------------------
        template <typename ST>
        class QuarticSolverCacheline
        {
	    public:
		typedef std::array<ST, 3> XYZPoint;
		
	    private:
		std::vector<XYZPoint>  EmissPntPool;
		std::vector<ST>  RadiusPool;
		std::vector<XYZPoint>  CoCPool;

            public:
// FIXME: const XYZPoint ?
		QuarticSolverCacheline(std::vector<XYZPoint> emissPntPool, 
				std::vector<ST> radiusPool, 
				std::vector<XYZPoint> cocPool) : 	EmissPntPool(emissPntPool), 
									RadiusPool(radiusPool),
									CoCPool(cocPool) { }

                // FIXME: performance penalty becaus of tuple?
                template< class T>
                inline std::tuple<T, T, T> transform(T evecX, T evecY, T evecZ, T dvecX, T dvecY, T dvecZ,
                                            T CoCX, T CoCY, T CoCZ, T sinbeta, T radius, T e) const {
                        T nx = (evecY*dvecZ) - (evecZ*dvecY);
                        T ny = (evecZ*dvecX) - (evecX*dvecZ);
                        T nz = (evecX*dvecY) - (evecY*dvecX);

                        const T norm = (nx*nx+ny*ny+nz*nz);
                        T norm_sqrt = approx_recipr(approx_rsqrt(norm));

                        // FIXME: is there a more performant asin function?
                        // FIXME: where else to add const?
                        // FIXME: alignment
                        // auto beta = asin_f(sinbeta);
                        const auto a = sinbeta*norm_sqrt;
                        INSERT_SSC_MARK(0x55555555);
                        const auto b = (1.0f-approx_recipr(approx_rsqrt(1.0f-(sinbeta*sinbeta))));
                        INSERT_SSC_MARK(0x66666666);
                        const auto enorm = radius*approx_recipr(e*norm);
                        // symmetric matrix, diagonal part is 0

                        const std::array<T, 9> M = {norm+b*(-nz*nz-ny*ny), a*nz+b*nx*ny, -a*ny+b*nx*nz,
                                                    -a*nz+b*nx*ny, norm+b*(-nx*nx-nz*nz), a*nx+b*ny*nz,
                                                    a*ny+b*nx*nz, -a*nx+b*ny*nz, norm+b*(-ny*ny-nx*nx)};


                        const auto ex = enorm*(evecX*M[0]+evecY*M[3]+evecZ*M[6]);
                        const auto ey = enorm*(evecX*M[1]+evecY*M[4]+evecZ*M[7]);
                        const auto ez = enorm*(evecX*M[2]+evecY*M[5]+evecZ*M[8]);

                        const T reflPointX = ex + CoCX;
                        const T reflPointY = ey + CoCY;
                        const T reflPointZ = ez + CoCZ;
                        return std::tuple<T, T, T>(reflPointX, reflPointY, reflPointZ);
                }

                /** Solves the characteristic quartic equation for the RICH optical system.
                 *
                 *  See note LHCB/98-040 RICH section 3 for more details
                 *
                 *  @param emissionPoint Assumed photon emission point on track
                 *  @param CoC           Spherical mirror centre of curvature
                 *  @param virtDetPoint  Virtual detection point
                 *  @param radius        Spherical mirror radius of curvature
                 *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
                 *
                 *  @return boolean indicating status of the quartic solution
                 *  @retval true  Calculation was successful. sphReflPoint is valid.
                 *  @retval false Calculation failed. sphReflPoint is not valid.
                 */
                template< class VECT, class SKALART >
                    inline void solve( VECTYPE_REDUCED::PhotonReflection<SKALART>& data) const
                    {

		/*	__builtin_prefetch(&(((&data)+0)->radius[0]), 0, 3);

			__builtin_prefetch(&(((&data+1)->emissPnt.x())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->emissPnt.y())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->emissPnt.z())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->centOfCurv.x())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->centOfCurv.y())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->centOfCurv.z())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->virtDetPoint.x())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->virtDetPoint.y())[0]), 0, 3);
			__builtin_prefetch(&(((&data+1)->virtDetPoint.z())[0]), 0, 3);


                        // TODO :align 64
                        // FIXME: ueberall const dranmachen?
                        VECT emissionPointVecX;
                        emissionPointVecX.load_a(&data.emissPnt.x()[0]);
                        VECT emissionPointVecY;
                        emissionPointVecY.load_a(&data.emissPnt.y()[0]);
                        VECT emissionPointVecZ;
                        emissionPointVecZ.load_a(&data.emissPnt.z()[0]);
		*/
// FIXME: aligned allocator for reduced photon reflections?
// FIXME: getter for x, y, z?
                        VECT emissionPointVecX = VECT(EmissPntPool[data.emissPnt_idx][0]);
                        VECT emissionPointVecY = VECT(EmissPntPool[data.emissPnt_idx][1]);
                        VECT emissionPointVecZ = VECT(EmissPntPool[data.emissPnt_idx][2]);

                        VECT CoCX = VECT(CoCPool[data.centOfCurv_rad_idx][0]);
                        VECT CoCY = VECT(CoCPool[data.centOfCurv_rad_idx][1]);
                        VECT CoCZ = VECT(CoCPool[data.centOfCurv_rad_idx][2]);

                        //const Vector evec( emissionPoint - CoC );
                        const VECT evecX = emissionPointVecX - CoCX;
                        const VECT evecY = emissionPointVecY - CoCY;
                        const VECT evecZ = emissionPointVecZ - CoCZ;

                        // vector from mirror centre of curvature to assumed emission point
                        // const TYPE e2 = evec.dot(evec);
                        const VECT e2 = evecX*evecX + evecY*evecY + evecZ*evecZ;

                        // vector from mirror centre of curvature to virtual detection point
                        VECT virtDetPointVecX;
                        virtDetPointVecX.load_a(&data.virtDetPoint.x()[0]);
                        VECT virtDetPointVecY;
                        virtDetPointVecY.load_a(&data.virtDetPoint.y()[0]);
                        VECT virtDetPointVecZ;
                        virtDetPointVecZ.load_a(&data.virtDetPoint.z()[0]);

                        // const Vector dvec( virtDetPoint - CoC );

                        const VECT dvecX = virtDetPointVecX - CoCX;
                        const VECT dvecY = virtDetPointVecY - CoCY;
                        const VECT dvecZ = virtDetPointVecZ - CoCZ;

                        // const TYPE d2 = dvec.dot(dvec);
                        const VECT d2 = dvecX*dvecX + dvecY*dvecY + dvecZ*dvecZ;

                        // various quantities needed to create quartic equation
                        // see LHCB/98-040 section 3, equation 3
                        //const auto ed2 = e2 * d2;
                        const VECT ed2 = e2 * d2;
                        const VECT evecDvec = evecX*dvecX + evecY*dvecY + evecZ*dvecZ;


                        // TODO: compare at this point
                        // TODO: div aufwändig? durch * 1/ed2 ersetzen?
                        // const TYPE cosgamma2 = ( ed2 > 0 ? std::pow(evec.dot(dvec),2)/ed2 : 1.0 );
                        // FIXME: durch square = pow of 2 ersetzen?
                        // approx_recipr(a); 
                        VECT cosgamma2 = (evecDvec * evecDvec)*approx_recipr(ed2);
                        cosgamma2 = select(ed2 > 0, cosgamma2, 1.0f);

                        // vectorise 4 square roots into 1
                        //using Vec4x = 
                        //  typename std::conditional<std::is_same<TYPE,float>::value,Vec4f,Vec4d>::type;
                        // const auto tmp_sqrt = sqrt( Vec4x( e2, d2,
                        //                                   cosgamma2 < 1.0 ? 1.0-cosgamma2 : 0.0,
                        //                                   cosgamma2 ) );
                        // const auto e         = tmp_sqrt[0];
                        // const auto d         = tmp_sqrt[1];
                        // const auto singamma  = tmp_sqrt[2];
                        // const auto cosgamma  = tmp_sqrt[3];

                        const VECT e = approx_recipr(approx_rsqrt(e2));
                        const VECT d = approx_recipr(approx_rsqrt(d2));

                        const VECT singamma = approx_recipr(approx_rsqrt(1.0f - cosgamma2));  // TODO: kann es echt > 1.0 werden?
                        const VECT cosgamma = approx_recipr(approx_rsqrt(cosgamma2));

                        // const auto dx        = d * cosgamma;
                        // const auto dy        = d * singamma;
                        // const auto r2        = radius * radius;
                        // const auto dy2       = dy * dy;
                        // const auto edx       = e + dx;
                        const VECT dx = d * cosgamma;
                        const VECT dy = d * singamma;
                        VECT radius = RadiusPool[data.centOfCurv_rad_idx];
                        const VECT r2 = radius * radius;
                        const VECT dy2 = dy * dy;
                        const VECT edx = e + dx;

                        // Fill array for quartic equation
                        // const auto a0      =     4.0 * ed2;
                        // const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
                        // const auto dyrad2  =     2.0 * dy * radius;
                        // const auto a1      = - ( 2.0 * dyrad2 * e2 ) * inv_a0;
                        // const auto a2      =   ( (dy2 * r2) + ( edx * edx * r2 ) - a0 ) * inv_a0;
                        // const auto a3      =   ( dyrad2 * e * (e-dx) ) * inv_a0;
                        // const auto a4      =   ( ( e2 - r2 ) * dy2 ) * inv_a0;

                        const VECT a0 = 4.0f * ed2;
                        // FIXME: warum floatmax, statt inf?
                        const VECT maxval  =  std::numeric_limits<SKALART>::max();
                        const VECT inv_a0    = select(a0 > 0, approx_recipr(a0), maxval);
                        const VECT dyrad2    = 2.0f * dy *radius;
                        const VECT a1        = -( 2.0f * dyrad2 * e2 ) * inv_a0;
                        const VECT a2        = ( ( dy2* r2 ) + (edx *edx * r2) - a0 ) * inv_a0;
                        const VECT a3        = ( (dyrad2 * e * (e-dx)) * inv_a0);
                        const VECT a4        = ( (e2 - r2) * dy2 ) * inv_a0;

                        // use simplified RICH version of quartic solver
                        TIMER_START(timer_quartic);
                        auto sinbeta = solve_quartic_RICH<VECT, SKALART>( a1, a2, a3, a4);
                        TIMER_STOP(timer_quartic);
                        PR0(sinbeta);

                        VECT reflPointX  __attribute__((__aligned__(64)));
                        VECT reflPointY  __attribute__((__aligned__(64)));
                        VECT reflPointZ __attribute__((__aligned__(64)));

                        // FIXME: pass by value or ref?

                        TIMER_START(timer_transform);
                        std::tuple<VECT, VECT, VECT> reflp =  transform<VECT>(evecX, evecY, evecZ,
                                                                            dvecX, dvecY, dvecZ,
                                                                            CoCX, CoCY, CoCZ,
                                                                            sinbeta, radius, e);
                        TIMER_STOP(timer_transform);

                        std::tie (reflPointX, reflPointY, reflPointZ) = reflp;
                        // (normalised) normal vector to reflection plane
                        // auto n = evec.cross3(dvec);
                        //   n /= std::sqrt( n.dot(n) );

                        /*
                        // construct rotation transformation
                        // Set vector magnitude to radius
                        // rotate vector and update reflection point
                        typedef Eigen::Matrix< TYPE , 3 , 1 > Eigen3Vector;
                        const Eigen::AngleAxis<TYPE> angleaxis( vdt::fast_asinf(sinbeta),
                        Eigen3Vector(n[0],n[1],n[2]) );
                        sphReflPoint = ( CoC + Gaudi::XYZVector( angleaxis *
                        Eigen3Vector(evec[0],evec[1],evec[2]) *
                        ( radius / e ) ) );
                        */

		    	__builtin_prefetch(&data.sphReflPoint.x()[0], 1, 0);
		    	__builtin_prefetch(&data.sphReflPoint.y()[0], 1, 0);
		    	__builtin_prefetch(&data.sphReflPoint.z()[0], 1, 0);
                        reflPointX.store_a(&data.sphReflPoint.x()[0]);
                        reflPointY.store_a(&data.sphReflPoint.y()[0]);
                        reflPointZ.store_a(&data.sphReflPoint.z()[0]);
                    }


                //----------------------------------------------------------------------
                /** Solves the quartic equation x^4 + a x^3 + b x^2 + c x + d = 0
                 *
                 *  Optimised to give only solutions needed by RICH optical system
                 *
                 *  Implemented using STL Complex numbers
                 *
                 *  @return The solution needed by the RICH
                 */
                //----------------------------------------------------------------------
                template < class VECT , class SKALART>
                    inline VECT solve_quartic_RICH ( const VECT& a, const VECT& b, const VECT& c, const VECT& d ) const
                    //  const TYPE& a,
                    //  const TYPE& b,
                    //  const TYPE& c,
                    //  const TYPE& d ) const 
                    {

                        const VECT r4 = VECT(1.0f / 4.0f);
                        const VECT q2 = VECT(1.0f / 2.0f);
                        const VECT q8 = VECT(1.0f / 8.0f);
                        const VECT q1 = VECT(3.0f / 8.0f);
                        const VECT q3 = VECT(3.0f / 16.0f);
                        //const VECT UU { -( std::sqrt((VECT)3.0) / (VECT)2.0 ) });
                        const VECT UU = VECT(-0.866025404); // - sqrt(3)/2

                        const auto aa = a * a;
                        const auto pp = b - q1 * aa;
                        const auto qq = c - (q2 * a * (b - (r4 * aa)));
                        const auto rr = d - r4 * (a * c - r4 * aa * (b - q3 * aa));
                        const auto rc = q2 * pp;
                        const auto sc = r4 * (r4 * pp * pp - rr);
                        const auto tc = -q8 * q8 * qq * qq;

                        const auto qcub = (rc * rc - 3 * sc);
                        const auto rcub = (2.0 * rc * rc * rc - 9 * rc * sc + 27.0 * tc);

                        const auto Q = qcub * Vec16f(1.0f/9.0f);
                        const auto R = rcub * Vec16f(1.0/54.0f);

                        const auto Q3 = Q * Q * Q;
                        const auto R2 = R * R;

                        // FIXME: saturated?
                        // const auto toberooted = (TYPE)( abs_saturated(R) + sqrt(abs_saturated(R2-Q3)) )
                        const auto toberooted = (abs(R) +  approx_recipr(approx_rsqrt(abs(R2-Q3)) ));

                        // FIXME: oder zuerst in normales array, dann load?
                        // FIXME: also for double?
                        // FIXME: magic numbers are eval
                        // FIXME: cbrt with one param of type Vec16f or Vec8d
                        // FIXME: 16 only for float
                        TIMER_START(timer_cbrt);
                        const auto rooted = cbrt(toberooted);
                        TIMER_STOP(timer_cbrt);

                        const auto A = select(R >= 0, -rooted, rooted);
                        PR0(A);

                        const auto B = Q * approx_recipr(A);

                        const auto u1 = -0.5 * (A + B) - rc * Vec16f(1.0f/3.0f);
                        // FIXME: saturated or not?
                        //const auto u2 = UU * abs_saturated(A-B);
                        const auto u2 = UU * abs(A-B);
                        const auto V =  approx_recipr(approx_rsqrt(u1*u1 + u2*u2));
                        // const std::complex<TYPE> w3 = ( abs_satured(V) != 0.0 ? (TYPE)( qq * -0.125 ) / V :
                        //                                std::complex<TYPE>(0,0) );
                        // FIXME: warum abs saturated when compared to 0.0 ??
                        const auto w3r = select(V != 0.0, (qq * -0.125)*approx_recipr(V), 0.0);
                        //        const TYPE res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);
                        const auto res =  approx_recipr(approx_rsqrt((u1+V)*2)) + w3r - (r4*a);
                        // return the final result
                        // FIXME: std::move ?
                        const auto r = select(res >  1.0,  1.0, select(res < -1.0, -1.0, res));
                        return r;
                    }

        };

    }
}

#endif // RICHRECPHOTONTOOLS_QuarticSolverNEW_H
